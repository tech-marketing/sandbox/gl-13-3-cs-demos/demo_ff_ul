package main

import (
	"auth/routes"
	"fmt"
	"encoding/base64"
	"crypto/tls"
	"crypto/x509"
	"log"
	"net/http"
	"os"
	"github.com/Unleash/unleash-client-go/v3"
)



func main() {

	
	// Get the SystemCertPool, continue with an empty pool on error
	rootCAs, _ := x509.SystemCertPool()
	if rootCAs == nil {
		rootCAs = x509.NewCertPool()
	}
	encodedCerts := os.Getenv("CA_PEM")
	data, err := base64.StdEncoding.DecodeString(encodedCerts)
	if err != nil {
		log.Fatal("error:", err)
	}
	certs := []byte(data)
	// Append our cert to the system pool
	if ok := rootCAs.AppendCertsFromPEM(certs); !ok {
		fmt.Println("No certs appended, using system certs only")
	}
	
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: false, RootCAs: rootCAs}
	unleash.Initialize(
		unleash.WithListener(&unleash.DebugListener{}),
		unleash.WithUrl(os.Getenv("UNLEASH_URL")),
		unleash.WithInstanceId(os.Getenv("UNLEASH_INSTANCE_ID")),
		unleash.WithAppName(os.Getenv("GITLAB_ENVIRONMENT_NAME")),
	)

	// Handle routes
	http.Handle("/", routes.Handlers())

	// serve
	log.Printf("Server up!")
	http.ListenAndServe(":5000", nil)
}
