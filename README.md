# Feature Flag Demo

## Overview

This demo shows how to use Feature Flags in GitLab. The application implements a RESTful API exposing basically 3 endpoints

+ /register
+ /login
+ /auth/message

An HTTP POST to ```/register``` is used to register a user account given some basic information most importantly ```email``` and  ```password``` posted using JSON formatted content.

An HTTP POST to ```/login``` logs in a given user with the ```password``` and returns a login token.

An HTTP GET to ```/auth/message``` with ```x-access-token: $TOKEN``` in the HTTp header will return a JSON formatted message depending on the feature flag enabled.

Please see the BASH script below for more detailed examples on how to use the endpoints.

### Demo code

The code is written in Golang to keep the resulting app small and fast.

### Auto DevOps

The demo uses Auto DevOps in order to build, test and deploy the app to a K8s cluster. A K8s cluster needs to be available for the project to use. Also, ensure to add the environment variable ```STAGING_ENABLED``` (set it to 1) to the project.

### Docker build

The build is performed by providing a Dockerfile that practically overrides a default Auto DevOps build. The Golang app is statically linked and copied to a docker image from scratch to keep the image as small as possible. The resulting image size is shy over 5 MB and has nothing in it other than the app.

### Embedded database

The demo code makes use of a Prostgres database to store user data. When the CI variable ```POSTGRES_ENABLED``` is true (Auto DevOps default) a Postgres instance is auto-deployed to the K8s cluster and the application pod will have automatically access to an envrironment variable called ```DATABASE_URL```. This variable is parsed in the golang code in order to connect to the database. There will a database instance per envrironment unless the code is changed to connect to an otherwise provided database. As a consequence the database is empty by default. Data access is automatically managed using [gorm](https://gorm.io/) for object relational mapping.

### Unleash

Feature flags are implemented using [unleash](https://github.com/Unleash/unleash). The service is embedded in the GitLab instance and the [client](https://github.com/Unleash/unleash-client-go) is used in the golang code. For the client to successfully connect to the service in GitLab the application pod needs to have an ```UNLEASH_URL``` and an ```UNLEASH_INSTANCE_ID``` environment variable. In order to pass these variable to the application pod, you need to add these variables in the GitLab CI variables section of your project by pre-fixing the variable names with ```K8S_SECRET_```. The required content is unique by GitLab project and available from the feature flag page.

### Secure connections

In order to make a secure connection from the application pod back to the unleash service embedded in the GitLab instance, the application pod needs to have access to the certificate of the certification authority used to secure the GitLab instance endpoint. That might be custom. In the demo project Let's Encrypt is used. The CA cert can be retrieved and base64 encoded with this command ```curl -s https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem.txt | base64```. The returned value needs to be stored in a GitLab CI variable named ```K8S_SECRET_CA_PEM``` for the application pod to make a trusted connection.

If you are using gitlab.com, Chrome, and macOS, follow these steps:

1. Point your browser to gitlab.com and ensure you are logged in to your gitlab account (so that you are not redirected to about.gitlab.com) and on your gitlab Projects Dashboard
2. Click on the padlock located right to the left of the URL
3. Scroll down and select "Certificate (Valid)"
4. Drag and drop the large Certificate icon, located on the left bottom panel of the pop-up, on your Desktop. Click on OK to dismiss the pop-up
5. Open the Certificate icon on your Desktop by double clicking on it. This will launch the "Keychain Access" application. Ensure that Window->Keychain Viewer is displayed
6. On the Keychain Viewer, select "Sectigo RSA Client Authentication and Secure Email CA" certificate, right click on it and export it in PEM format
7. Open a Terminal window and change directory to wherever you exported the certificate from the previous step
8. Enter the command ```cat Sectigo\ RSA\ Client\ Authentication\ and\ Secure\ Email\ CA.pem | base64```

The returned value needs to be stored in a GitLab CI variable named ```K8S_SECRET_CA_PEM``` for the application pod to make a trusted connection.

### Test client

A bash test client using the 3 endpoints looks like this. For the script to work [jq](https://stedolan.github.io/jq/) needs to be available on the client computer.

```bash
#!/bin/sh
# here we loop over our environments
for BASEURL in https://cluster-feature-flags-demo-staging.apps.gitlab.ninja
do
    # checking whether environment is reachable
    if curl -s --head  --request GET $BASEURL | grep "200" > /dev/null; then
        echo "$BASEURL is UP"
        # list of users
        for EMAIL in user1@example.com user2@example.com user3@example.com user4@example.com
        do
            echo User=$EMAIL
            PASSWORD=secret
            # let's register the user just in case and ignore the output
            curl -s -d '{"Email":"'$EMAIL'","Password":"'$PASSWORD'"}' -H "Content-Type: application/json" -X POST $BASEURL/register > /dev/null
            # here we are logging in and storing the access token
            TOKEN=`curl -s -d '{"Email":"'$EMAIL'","Password":"'$PASSWORD'"}' -H "Content-Type: application/json" -X POST $BASEURL/login | jq -r '.token'`
            [ -z "$TOKEN" ] && echo "Can't log in" || for i in 1
            do
                # here we are getting the message
                curl -s -H "Accept: application/json" -H "Content-Type: application/json" -H "x-access-token: $TOKEN" $BASEURL/auth/message | python -m json.tool
                #sleep 1
            done
            #sleep 1
        done
    else
        echo "$BASEURL is DOWN"
    fi
done
```

### Implementing a feature flag

To implement a feature flag code has to be changed and the feature flag has to be defined in the GitLab project.

Code changes go in ```controllers/user.go```. The name of the flag in the code has to match the name of the flag in GitLab.

### Testing a feature flag

The application can be tested with the forementioned BASH script. The script needs the base URL(s) of the environment(s) on line 3. It will register the users on line 9. The user emails can be used to drive the activation strategies for the feature flag in GitLab.
